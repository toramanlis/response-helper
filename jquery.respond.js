(function($){

    var listener,curEnv;

    function arrDiff(a1, a2)
    {
        var a = [], diff = [];
        for (var i = 0; i < a1.length; i++){
            a[a1[i]] = true;
        }

        for (var i = 0; i < a2.length; i++){
            if (a[a2[i]]){
                delete a[a2[i]];
            }
        }

        for (var k in a)
        {
            if (a[k] === true){
                diff.push(k);
            }
        }

        return diff;
    }

    function saveAttr(env)
    {
        $('[data-'+env+'-style], [data-'+env+'-attr], [data-'+env+'-class]').each(function(){

            $this=$(this);

            if($this.is('[data-'+env+'-style]'))
            {
                var defaultAttr=eval('('+($this.attr('data-default-attr') || '{}')+')');
                if(defaultAttr.style===undefined)
                {
                    var style=$this.attr('style') || '';
                    style+=(!style || style[style.length-1]==';')?'':';';

                    defaultAttr.style=style;
                    $this.attr('data-default-attr',JSON.stringify(defaultAttr));
                }
            }

            if($this.is('[data-'+env+'-attr]'))
            {
                var defaultAttr=eval('('+($this.attr('data-default-attr') || '{}')+')');
                var dataAttr=eval('('+$this.attr('data-'+env+'-attr')+')');

                for(var key in dataAttr)
                {
                    if(defaultAttr[key]===undefined)
                    {
                        defaultAttr[key]=$this.attr(key) || '';
                    }
                }

                $this.attr('data-default-attr',JSON.stringify(defaultAttr));

            }

            if($this.is('[data-'+env+'-class]'))
            {
                var defaultAttr=eval('('+($this.attr('data-default-attr') || '{}')+')');
                if(defaultAttr.class===undefined)
                {
                    defaultAttr.class=$this.attr('class');
                    $this.attr('data-default-attr',JSON.stringify(defaultAttr));
                }
            }
        });
    }

    function changeStyle($elem,env)
    {
        var style=$elem.attr('style') || '';
        style+=(!style || style[style.length-1]==';')?'':';';
        $elem.attr('style',style+$elem.attr('data-'+env+'-style'));
    }

    function addClass(elem,callbacks,cl,env){

        if(typeof cl !="string")
        {
            return;
        }

        var callbackExists=(callbacks && callbacks[cl] && callbacks[cl].onAdd);

        if(callbackExists)
        {
            var callbackReturns=(callbacks[cl].onAdd.apply(elem,[cl,env,curEnv])!==false);
        }

        if(!callbackExists || callbackReturns)
        {
            try{
                $(elem).addClass(cl);
            }
            catch(e){}
        }
    }

    function removeClass(elem,callbacks,cl,env){

        if(typeof cl !="string")
        {
            return;
        }

        var callbackExists=(callbacks && callbacks[cl] && callbacks[cl].onRemove);


        if(callbackExists)
        {
            var callbackReturns=(callbacks[cl].onRemove.apply(elem,[cl,env,curEnv])!==false);
        }

        if(!callbackExists || callbackReturns)
        {
            try{
                $(elem).removeClass(cl);
            }
            catch(e){}
        }
    }

    function changeAttr($elem,env,callbacks)
    {
        var attrs=eval('('+$elem.attr('data-'+env+'-attr')+')');

        for(var attr in attrs)
        {
            var value=attrs[attr];

            if(attr=='class' && callbacks)
            {
                var current=$elem.attr('class').replace(/\s+/,' ').split(' ');
                var change=value.replace(/\s+/,' ').split(' ');
                var toAdd=arrDiff(change,current);
                var toRemove=arrDiff(current,change);

                for(var i in toAdd)
                {
                    var cl=toAdd[i];
                    addClass(this,callbacks,cl,env);
                }

                for(i in toRemove)
                {
                    var cl=toRemove[i];
                    removeClass(this,callbacks,cl,env);
                }
            }
            else
            {
                $elem.attr(attr,value);
            }
        }
    }


    function changeClass($elem,env,callbacks){

        var envCl=($elem.attr('data-'+env+'-class') || '').replace(/\s+/,' ').split(" ");

        for(var i in envCl)
        {
            if(envCl[i][0]=='!' && $elem.hasClass(envCl[i].substr(1)))
            {
                var cl=envCl[i].substr(1);
                removeClass(this,callbacks,cl,env);
            }
            else if(envCl[i][0]!='!' && !$elem.hasClass(envCl[i]))
            {
                var cl=envCl[i];
                addClass(this,callbacks,cl,env);
            }
        }
    }

    function respond(env,callbacks)
    {
        if(env!='default')
        {
            saveAttr(env);
        }

        $('[data-'+curEnv+'-style], [data-'+curEnv+'-attr], [data-'+curEnv+'-class]').each(function(){
            var $this=$(this);
            if($this.is('[data-default-style]'))
            {
                changeStyle.apply(this,[$this,'default']);
            }

            if($this.is('[data-default-attr]'))
            {
                changeAttr.apply(this,[$this,'default',callbacks]);
            }

            if($this.is('[data-default-class]'))
            {
                changeClass.apply(this,[$this,'default',callbacks]);
            }
        });

        $('[data-'+env+'-style], [data-'+env+'-attr], [data-'+env+'-class]').each(function(){
            var $this=$(this);
            if($this.is('[data-'+env+'-style]'))
            {
                changeStyle.apply(this,[$this,env]);
            }

            if($this.is('[data-'+env+'-attr]'))
            {
                changeAttr.apply(this,[$this,env,callbacks]);
            }

            if($this.is('[data-'+env+'-class]'))
            {
                changeClass.apply(this,[$this,env,callbacks]);
            }
        });

        curEnv=env;
    }

    function apply(conf)
    {
        var width=$('body').width();
        var match=false;

        for(var name in conf.envs)
        {
            var env=conf.envs[name];
            if((env.min===undefined || width>=env.min) && (env.max===undefined || width<=env.max))
            {
                match=true;
                if(curEnv!=name)
                {
                    respond(name,conf.callbacks);
                }
            }
        }

        if(!match && curEnv!='default')
        {
            respond('default',conf.callbacks);
        }
    }

    function start(conf)
    {
        listener=setInterval(function(){
            apply(conf);
        },(conf.interval || 500))
    }

    function stop(){
        clearInterval(listener);
    }

    $.respond=function(param,listen){
        switch(typeof param)
        {
            case 'string':
                respond(param);
                break;
            case 'object':
                if(listen)
                {
                    start(param);
                }
                else
                {
                    apply(param);
                }
                break;
            default:
                if(!param)
                {
                    stop();
                }
                break;
        }
    };
})(jQuery);